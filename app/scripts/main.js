function replaceAmp(url) {
    return url.replace(/&amp;/g, "&");
}
function contactFormUk() {
	var name = $('#name').val();
	var email = $('#email').val();
	var company = $('#company').val();
	var phone = $('#phone').val();

	if (name.length > 1 && company.length>1 && phone.length>1 && phone.length >1 && validateEmail(email) == false) {
		$.post('http://www.arobs.com/index.php?id=85&type=1000', "&captcha="+$("#g-recaptcha-response").val()+"&todo=CAPTCHA_VERIFICATION", function(result){
			if (result == 1) {
				$('#contactForm').submit();
			} else {
				$('#captcha').addClass('error');
			}
		});
		
	}else {
		if(name.length == 0) {
			$('#name').addClass('error');
		}else{
			$('#name').removeClass('error');
		}
		if(email.length == 0 || validateEmail(email) == false) {
			$('#email').addClass('error');
		}else{
			$('#email').removeClass('error');
		}
		if(company.length == 0) {
			$('#company').addClass('error');
		}else{
			$('#company').removeClass('error');
		}
		if(phone.length == 0 ) {
			$('#phone').addClass('error');
		}else{
			$('#phone').removeClass('error');
		}
		
	}
}
function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

var UI_getSmallest = function(collection) {
	var heightArray = [];
	var selectedElement = $(collection);
	selectedElement.each(function() {
		heightArray.push(parseInt($(this).height(), 10));
	});

	return Math.min.apply(Math, heightArray);
};

function getScope(ctrlName) {
    var sel = 'div[ng-controller="' + ctrlName + '"]';
    return angular.element(sel).scope();
}

var UI_setTallestHeightBox = function (collection) {
	$(collection).height('auto');
	var tallest = 0;
	var thisHeight;
	var selectedElement = $(collection);
	selectedElement.each(function() {
		thisHeight = $(this).height();
		if(thisHeight > tallest) {
			tallest = thisHeight;
		}
	});
	selectedElement.height(tallest);
}

function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}


$(document).ready(function(){

	var languages = ["English", "Magyar", "Deutch"];

	$("#countries").msDropdown();

	var languageToLoad = getCookie('selectedLanguage');

	var languageList = $("#countries_child ul li");
	if (languageToLoad == "English")
	{
		$(languageList[0]).addClass("selected");
		$(languageList[1]).removeClass("selected");
		$(languageList[2]).removeClass("selected");
	} else if (languageToLoad == "Magyar")
	{
		$(languageList[1]).addClass("selected");
		$(languageList[0]).removeClass("selected");
		$(languageList[2]).removeClass("selected");

		$("#countries_title img").removeClass("us");
		$("#countries_title img").addClass("hu");

	} else if (languageToLoad == "Deutch")
	{
		$(languageList[2]).addClass("selected");
		$(languageList[1]).removeClass("selected");
		$(languageList[0]).removeClass("selected");
		$("#countries_title img").removeClass("us");
		$("#countries_title img").addClass("de");
	}
	else
	{
		$(languageList[0]).addClass("selected");
		$(languageList[1]).removeClass("selected");
		$(languageList[2]).removeClass("selected");
	}


	$("#countries_title .ddlabel").text(languageToLoad);

	$("#countries_child ul").on("click", function(e){
		//alert($(event.target).hasClass("selected"));

        var t = this.children;
        var selectedLanguage = 0;
        for (var i=0; i<t.length; i++)
        {
        	if ($(t[i]).hasClass("selected"))
        	{
        		selectedLanguage = i;
        		break;
        	}
        }
        
        document.cookie = "selectedLanguage = " + languages[selectedLanguage];

        location.reload();
	});

	<!-- auto-generate carousel indicator html -->
	var myCarousel = $(".carousel");
	myCarousel.append("<ol class='carousel-indicators'></ol>");
	var indicators = $(".carousel-indicators"); 
	myCarousel.find(".carousel-inner").children(".item").each(function(index) {
		(index === 0) ? 
		indicators.append("<li data-target='#carousel-example-generic' data-slide-to='"+index+"' class='active'></li>") : 
		indicators.append("<li data-target='#carousel-example-generic' data-slide-to='"+index+"'></li>");
	});     
	$('#carouselPages').carousel({
		interval: false
	}); 
	$('#carouselPages1').carousel({
		interval: false
	}).on('slide', function () {
    	var titleHeight = parseInt($('.active .contactTxt h2').height());
		$('.active .contactTxt').height(titleHeight + 40);
	});  
	$('.fWidthBtn').on("click", function(){
		$('#servicesPopUp').modal('hide');
		$('#myModalCase').modal('hide');
	});
	$('#header-click').on("click", function(){
		window.open('http://www.arobs.com/about/it-events/wtm-london-2014/','_blank');
	});
	$(".imgOverlay").mouseenter(function(){
		var self = $(this);
		self.addClass('imageOverlayHover');

	}).mouseleave(function(){
		var self = $(this);
		self.removeClass('imageOverlayHover');
	});
	
	
	$(".slideBtns .transition-all").mouseenter(function(){
		var self = $(this);
		self.addClass('btnHover');

	}).mouseleave(function(){
		var self = $(this);
		self.removeClass('btnHover');
	});
	
	$('.closeBtn').on("click", function(){
		$('#carouselPages1 .item').removeClass("active");
		$('#carouselPages .item').removeClass("active");
		$('#myModal').modal('hide');

		$('#myModalCase').modal('hide');
		$('#servicesPopUp').modal('hide');
	});
	
	$('.services').on("click", function(){
		$('#buttonServices').click();
		$('#carouselPages1 #'+this.id).addClass('active');
		$('#carouselPages1 #'+this.id).removeClass('next');
		$('#carouselPages1 #'+this.id).removeClass('left');
	});

	setHight = function() {
		var smallestHeight = UI_getSmallest('.casesContainer .imgBoxContainer img');
		$('.casesContainer .imgBoxContainer').height(smallestHeight);
	}

	UI_setTallestHeightBox('ul.equalResultsColumns li');
	
		setTimeout(function() {
      		setHight();
		}, 1600);
	
	
	
	$(window).on('resize', function(){
		setHight();
		UI_setTallestHeightBox('ul.equalResultsColumns li');
	});

	$('#servicesPopUp a[data-slide="prev"]').click(function() {
		  $('#carouselPages1').carousel('prev');
		 });

		 $('#servicesPopUp a[data-slide="next"]').click(function() {
		  $('#carouselPages1').carousel('next');
		 });
		 $('#myModalCase a[data-slide="prev"]').click(function() {
		  $('#carouselPages').carousel('prev');
		 });

		 $('#myModalCase a[data-slide="next"]').click(function() {
		  $('#carouselPages').carousel('next');
		 });

	var thisYear = new Date().getFullYear();
	$('#currentYear').text(thisYear);

	$("#language").click(function(){
	   $("#lang li").slideToggle();
	});

	$("#lang li").click(function() {

	});

});