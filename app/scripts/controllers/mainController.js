'use strict';

App.Controllers.MainController = ['$scope','$state', '$rootScope', '$http', function ($scope, $state, $rootScope, $http) {
   $scope.mainViewModel = new MainViewModel($state, $http);
}];

function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

function MainViewModel($state, $http) {

	var cookies = document.cookie;

	var selectedLanguage = getCookie('selectedLanguage');

	var self = this;

	var fileToRead = 'app/internationalization/english.json';
	if (selectedLanguage == 'English')
		fileToRead = 'app/internationalization/english.json';
	else if (selectedLanguage == "Magyar")
		fileToRead = 'app/internationalization/magyar.json';
	else if (selectedLanguage == "Deutch")
		fileToRead = 'app/internationalization/deutch.json';


	$http.get(fileToRead).success(function(data) {
		var result = data;
		self.welcomeText = angular.fromJson(result.welcomeText);
		self.images = angular.fromJson(result.images);
		self.contactInfo = angular.fromJson(result.contactInfo);
		self.services = angular.fromJson(result.services);
		self.service1 = angular.fromJson(result.service1);
		self.service2 = angular.fromJson(result.service2);
		self.service3 = angular.fromJson(result.service3);
		self.works = angular.fromJson(result.works);
		self.workList = angular.fromJson(result.workList);
		self.brands = angular.fromJson(result.brands);
		self.footerInfo = angular.fromJson(result.footerInfo);
		self.address = angular.fromJson(result.address);
	});
}